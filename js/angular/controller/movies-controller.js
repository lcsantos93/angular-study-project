angular.module('cwiTest').controller('MoviesController', function($scope, $http, $httpParamSerializer) {

  console.log("running listing event controller");

  var REQ = {
    url: 'http://private-74b50-provaangularjs.apiary-mock.com/movies',
    method: 'GET'
  };

  function getMovies(REQ) {
    $http(REQ)
    .success(function(data){
      console.log('Data: ', data);
      $scope.carrossel = data.carrossel;
      $scope.catalogo = data.catalogo;
      console.log('Data: ', data);
    })
    .error(function(err){
      console.log('Erro: ', err);
    });
  }

  getMovies(REQ);

  $scope.findMovies = function(){

    $http.get("http://private-74b50-provaangularjs.apiary-mock.com/movies/" + $scope.searchResults).success(function(data){
      $scope.name = data.catalogo.name;
      $scope.descricao = data.catalogo.descricao;


    })
    }


});