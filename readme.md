# project-movies

This project is a technical evaluation for the position of Front-End Developer for the company CWI Software.

## Getting Started

Run npm install.

## Build With

* [Angular JS] (http://devdocs.io/angularjs~1.4/) - The web framework used
* [Grunt] (https://gruntjs.com/) - The JavaScript Task Runner used
* [Less] (lesscss.org/) - CSS pre-processor
* [Npm] (https://www.npmjs.com/) - The package manager used


## Author

Lucas Cardoso Santos - Front-end Web Developer.